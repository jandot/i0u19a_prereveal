# I0U19A - Management of Large Omics Datasets

This repository holds slides and exercises for the course I0U19A of Master of Bioinformatics at KU Leuven, Belgium.

Lecturer: Jan Aerts
Teaching Assistants: Toni Verbeiren, Ryo Sakai, Raf Winand, Thomas Moerman

This repo should be run as a subtree of bitbucket.org/jandot/public_html
