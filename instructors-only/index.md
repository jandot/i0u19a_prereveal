---
title: Instructor-only section for I0U19A
layout: page
---
* Answers to SQL assignment: [here](RDBMS-answers.html)
* Data processing answers: [here](data-processing-answers.html)
* MongoDB answers: [here](mongodb-answers.html)
