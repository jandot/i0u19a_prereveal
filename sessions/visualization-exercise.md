---
title: Visualization - exercise
layout: page
---
# Feedback and issues with P5
Goal: answer questions of students regarding use of P5.

# Designing and implementing new visualizations
Goal: discuss some of the designs submitted by students for the 5-design sheets for the flight data. => go through new round of design => start implementation

Other option: use police violence data from [http://vizwiz.blogspot.be/p/makeover-monday-challenges.html](http://vizwiz.blogspot.be/p/makeover-monday-challenges.html)
