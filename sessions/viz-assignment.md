---
title: Data visualization assignment
layout: page
---
This assignment should be uploaded to Toledo before the data visualization exercise session.

# P5 tutorial
For the first part of the assignment, please follow the tutorial [here]({{ site.baseurl }}/2015/10/hands-on-data-visualization-using-p5/). This tutorial will work you through creating visualizations in [P5](http://p5js.org), which is a javascript-version of [Processing](http://processing.org). For the assignment, go through the complete tutorial, and upload a picture of the flights data, where you use lines that connect the departure with the arrival airports.

# Visual design
For the second part of the assignment, read the 5-design sheet methodology paper (see [here]({{ site.baseurl }}/teaching/i0u19a/assets/fds.pdf)). Then suppose that the user is interested in the **relationships between countries** (e.g. the number of flights between them, which country the airline is registered in, the distance between those countries, etc). Create 5 sheets where you explore this visually. Take a picture of these or scan them, and upload on Toledo.
